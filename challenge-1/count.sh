#!/bin/sh

file="00-example-access.log"
count=$(grep -c "HTTP/1\.1\" 500" "$file")

echo "Number of HTTP 500 errors in $file: $count"
